const path = require('path');

module.exports = {
  "stories": [
    "../app/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-create-react-app",
    '@storybook/addon-actions'
  ],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need
    config.module.rules.push({
      test: [/\.(png|woff|woff2|eot|ttf|svg|jpg)$/],
      use: ['file-loader'],
      include: path.resolve(__dirname, '../'),
    });

    config.module.rules.push({
      test: [/\.css$/,/\.sass$/,/\.scss$/],
      use: ["style-loader","css-loader", "sass-loader"],
      include: [
        path.resolve(__dirname, "../app"),
        path.resolve(__dirname, "../node_modules/react-awesome-slider"),
        path.resolve(__dirname, "../node_modules/ag-grid-community/dist/styles/ag-grid.css"),
        path.resolve(__dirname, "../node_modules/ag-grid-community/dist/styles/ag-theme-alpine.css"),


      ]
    });

    // Return the altered config
    return config;
  },
}