module.exports = {
    description: 'Add an container component',
    prompts: [
        {
            type:'input',
            name:'name',
            message:'What should it be called?',
            default:'Button',
        },
    ],
    actions : data => {
        const actions = [
            {
                type:'add',
                path:'../../app/scenes/{{properCase name}}/index.ts',
                templateFile:'./scene/index.ts.hbs',
                abortOnFail:true,
            }
        ];
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/styles.ts',
            templateFile:'./scene/styles.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/messages.tsx',
            templateFile:'./scene/messages.tsx.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/actions.ts',
            templateFile:'./scene/actions.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/reducer.ts',
            templateFile:'./scene/reducer.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/constants.ts',
            templateFile:'./scene/constants.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/saga.ts',
            templateFile:'./scene/saga.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/selectors.ts',
            templateFile:'./scene/selectors.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/scenes/{{properCase name}}/layout.tsx',
            templateFile:'./scene/layout.tsx.hbs',
            abortOnFail:true,
        });
        // actions.push({
        //     type:'prettify',
        //     path:'/components/'
        // })

        return actions;
    }

}