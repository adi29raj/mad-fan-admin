module.exports = {
    description: 'Add an unconnected component',
    prompts: [
        {
            type:'input',
            name:'name',
            message:'What should it be called?',
            default:'Button',
        },
    ],
    actions : data => {
        const actions = [
            {
                type:'add',
                path:'../../app/components/{{properCase name}}/index.tsx',
                templateFile:'./component/index.tsx.hbs',
                abortOnFail:true,
            }
        ];
        actions.push({
            type:'add',
            path:'../../app/components/{{properCase name}}/styles.ts',
            templateFile:'./component/styles.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/components/{{properCase name}}/messages.tsx',
            templateFile:'./component/messages.ts.hbs',
            abortOnFail:true,
        });
        actions.push({
            type:'add',
            path:'../../app/components/{{properCase name}}/index.stories.tsx',
            templateFile:'./component/stories.tsx.hbs',
            abortOnFail:true,
        });
        // actions.push({
        //     type:'prettify',
        //     path:'/components/'
        // })

        return actions;
    }

}