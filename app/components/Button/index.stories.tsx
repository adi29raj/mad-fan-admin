/**
 *
 * Button Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";

import Button from ".";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

const stories = storiesOf("Button", module);

stories.add("Button Type", () => (
  <div style={{ display: "flex", justifyContent: "space-around" }}>
    <Button name="Primary" buttonType="primary" />
    <Button name="Secondary" buttonType="secondary" />
  </div>
));

stories.add("Button Size", () => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-around",
      alignItems: "center",
    }}
  >
    <Button name="Regular" buttonSize="regular" />
    <Button name="Small" buttonSize="small" />
  </div>
));

stories.add("Selected + With Icon", () => (
  <Button name="Regular" buttonSize="regular" selected leftIcon={faAngleLeft} />
));
