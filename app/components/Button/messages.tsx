/**
* Button Messages
*
* This contains all the text for the Button Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.Button';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})