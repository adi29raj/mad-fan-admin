/**
 *
 * CelebNavigation Component
 *
 */
import React, { useState } from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import logo from "../../images/logo.png";
import bell from "../../images/bell.png";

import {
  faThLarge,
  faUser,
  faEdit,
  faEnvelopeOpenText,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@material-ui/core";
import { CelebRoute } from "./enums";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;

  children: any;

  device: string;

  route?: string;

  celebName: string;

  noOfNotifs: number;

  dp: any;

  onRouteChange: (route: string) => void;

  onNotifClick: () => void;

  onProfileClick: () => void;
};

function CelebNavigation(props: Props) {
  const {
    classes,
    style,
    className,
    formatMessage,
    device = "large",
    route = CelebRoute.Dashboard,
    celebName,
    noOfNotifs,
    dp,
    children,
    onRouteChange,
    onNotifClick,
    onProfileClick,
  } = props;

  const [curRoute, setCurRoute] = useState(route);

  const getRouteName = () => {
    if (curRoute === CelebRoute.Dashboard)
      return formatMessage(messages.dashboard);
    if (curRoute === CelebRoute.Requests)
      return formatMessage(messages.fanRequest);
    if (curRoute === CelebRoute.Profile) return formatMessage(messages.profile);
  };

  return (
    <div className={classnames(classes.container, className)} style={style}>
      {device === "large" ? (
        <div>
          <div className={classes.largeDevice}>
            <section className={classes.leftBar}>
              <img src={logo} alt="Logo" className={classes.logo} />
              <div className={classes.wrap}>
                <Button
                  onClick={() => {
                    setCurRoute(CelebRoute.Dashboard);
                    onRouteChange(CelebRoute.Dashboard);
                  }}
                  className={classnames(classes.iconBtn, {
                    [classes.iconBtnSelected]:
                      curRoute === CelebRoute.Dashboard,
                  })}
                >
                  <div className={classes.iconWrap}>
                    <FontAwesomeIcon
                      className={classnames(classes.icon, {
                        [classes.iconSelected]:
                          curRoute === CelebRoute.Dashboard,
                      })}
                      icon={faThLarge}
                    />
                    <div
                      className={classnames(classes.text, {
                        [classes.textSelected]:
                          curRoute === CelebRoute.Dashboard,
                      })}
                    >
                      {formatMessage(messages.dashboard)}
                    </div>
                  </div>
                </Button>
                <Button
                  className={classnames(classes.iconBtn, {
                    [classes.iconBtnSelected]: curRoute === CelebRoute.Requests,
                  })}
                  onClick={() => {
                    setCurRoute(CelebRoute.Requests);
                    onRouteChange(CelebRoute.Requests);
                  }}
                >
                  <div className={classes.iconWrap}>
                    <FontAwesomeIcon
                      className={classnames(classes.icon, {
                        [classes.iconSelected]:
                          curRoute === CelebRoute.Requests,
                      })}
                      icon={faEnvelopeOpenText}
                    />
                    <div
                      className={classnames(classes.text, {
                        [classes.textSelected]:
                          curRoute === CelebRoute.Requests,
                      })}
                    >
                      {formatMessage(messages.fanRequest)}
                    </div>
                  </div>
                </Button>
                <Button
                  className={classnames(classes.iconBtn, {
                    [classes.iconBtnSelected]: curRoute === CelebRoute.Profile,
                  })}
                  onClick={() => {
                    setCurRoute(CelebRoute.Profile);
                    onRouteChange(CelebRoute.Profile);
                  }}
                >
                  <div className={classes.iconWrap}>
                    <FontAwesomeIcon
                      className={classnames(classes.icon, {
                        [classes.iconSelected]: curRoute === CelebRoute.Profile,
                      })}
                      icon={faUser}
                    />
                    <div
                      className={classnames(classes.text, {
                        [classes.textSelected]: curRoute === CelebRoute.Profile,
                      })}
                    >
                      {formatMessage(messages.profile)}
                    </div>
                  </div>
                </Button>
              </div>
            </section>
            <section className={classes.rightBar}>
              <section className={classes.topBar}>
                <section className={classes.left}>
                  <div className={classes.routeName}>{getRouteName()}</div>
                </section>
                <section className={classes.right}>
                  <Button
                    className={classes.notifButton}
                    onClick={onNotifClick}
                    size="large"
                  >
                    <img
                      src={bell}
                      alt="Notification Bell"
                      className={classes.notifImg}
                    />
                    <div className={classes.noOfNotifs}>{noOfNotifs}</div>
                  </Button>
                  <Button
                    className={classes.dp}
                    onClick={onProfileClick}
                    size="large"
                  >
                    <img
                      src={dp}
                      alt="Profile"
                      className={classes.displayImg}
                    />
                  </Button>
                  <Button className={classes.celebName}>{celebName}</Button>
                </section>
              </section>
              <section className={classes.content}>{children}</section>
            </section>
          </div>
        </div>
      ) : (
        <div className={classes.smallDevice}>
          <section className={classes.top}>
            <img src={logo} alt="Logo" className={classes.logoSmall} />
            <Button
              className={classes.notifButtonSmall}
              onClick={onNotifClick}
              size="small"
            >
              <img
                src={bell}
                alt="Notification Bell"
                className={classes.notifImgSmall}
              />
              <div className={classes.noOfNotifsSmall}>{noOfNotifs}</div>
            </Button>
          </section>
          <section className={classes.middle}>
            <div className={classes.routeNameSmall}>{getRouteName()}</div>
            <div className={classes.contentSmall}>{children}</div>
          </section>
          <section className={classes.bottom}>
            <Button
              onClick={() => {
                setCurRoute(CelebRoute.Dashboard);
                onRouteChange(CelebRoute.Dashboard);
              }}
              className={classes.iconBtnSmall}
            >
              <div className={classes.iconWrapSmall}>
                <FontAwesomeIcon
                  className={classnames(classes.iconSmall, {
                    [classes.iconSelectedSmall]:
                      curRoute === CelebRoute.Dashboard,
                  })}
                  icon={faThLarge}
                />
                <div
                  className={classnames(classes.textSmall, {
                    [classes.textSelectedSmall]:
                      curRoute === CelebRoute.Dashboard,
                  })}
                >
                  {formatMessage(messages.dashboard)}
                </div>
              </div>
            </Button>
            <Button
              className={classes.iconBtnSmall}
              onClick={() => {
                setCurRoute(CelebRoute.Requests);
                onRouteChange(CelebRoute.Requests);
              }}
            >
              <div className={classes.iconWrapSmall}>
                <FontAwesomeIcon
                  className={classnames(classes.iconSmall, {
                    [classes.iconSelectedSmall]:
                      curRoute === CelebRoute.Requests,
                  })}
                  icon={faEnvelopeOpenText}
                />
                <div
                  className={classnames(classes.textSmall, {
                    [classes.textSelectedSmall]:
                      curRoute === CelebRoute.Requests,
                  })}
                >
                  {formatMessage(messages.fanRequest)}
                </div>
              </div>
            </Button>
            <Button
              className={classes.iconBtnSmall}
              onClick={() => {
                setCurRoute(CelebRoute.Profile);
                onRouteChange(CelebRoute.Profile);
              }}
            >
              <div className={classes.iconWrapSmall}>
                <FontAwesomeIcon
                  className={classnames(classes.iconSmall, {
                    [classes.iconSelectedSmall]:
                      curRoute === CelebRoute.Profile,
                  })}
                  icon={faUser}
                />
                <div
                  className={classnames(classes.textSmall, {
                    [classes.textSelectedSmall]:
                      curRoute === CelebRoute.Profile,
                  })}
                >
                  {formatMessage(messages.profile)}
                </div>
              </div>
            </Button>
          </section>
        </div>
      )}
    </div>
  );
}

export default withStyles(styles)(withIntl(CelebNavigation));
