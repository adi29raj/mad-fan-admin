/**
 *
 * CelebNavigation Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import sample from "../../images/sample.png";
import BookVideoForm from "../BookVideoForm";
import CelebNavigation from ".";

const stories = storiesOf("CelebNavigation", module);

stories.add("default", () => (
  <CelebNavigation
    device="large"
    noOfNotifs={5}
    dp={sample}
    celebName="Calicdoo"
    // route="profile"
    onRouteChange={action("Route Change")}
    onNotifClick={action("Notif clicked")}
    onProfileClick={action("Profile Clicked")}
  >
    Hello
  </CelebNavigation>
));

stories.add("mobile", () => (
  <CelebNavigation
    device="small"
    noOfNotifs={5}
    dp={sample}
    celebName="Calicdoo"
    // route="profile"
    onRouteChange={action("Route Change")}
    onNotifClick={action("Notif clicked")}
    onProfileClick={action("Profile Clicked")}
  >
    Hello
  </CelebNavigation>
));
