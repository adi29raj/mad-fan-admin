/**
 * CelebNavigation Messages
 *
 * This contains all the text for the CelebNavigation Component
 *
 */

import { defineMessages } from "react-intl";

export const scope = "app.component.CelebNavigation";

export default defineMessages({
  dashboard: {
    id: `${scope}.dashboard`,
    defaultMessage: "Dashboard",
  },
  fanRequest: {
    id: `${scope}.fanRequest`,
    defaultMessage: "Fan Request",
  },
  profile: {
    id: `${scope}.profile`,
    defaultMessage: "Profile",
  },
});
