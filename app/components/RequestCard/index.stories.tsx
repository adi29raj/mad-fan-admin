/**
 *
 * RequestCard Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import RequestCard from ".";

const stories = storiesOf("RequestCard", module);

stories.add("Default", () => (
  <div
    style={{
      backgroundColor: "#f3f6fd",
      display: "flex",
      justifyContent: "space-between",
      flexWrap: "wrap",
    }}
  >
    <RequestCard size="large" number={23} requestType="new" />

    <RequestCard size="large" number={23} requestType="pending" />

    <RequestCard size="large" number={2} requestType="all" />

    <RequestCard size="large" number={2} requestType="clients" />
  </div>
));

stories.add("mobile", () => (
  <div
    style={{
      backgroundColor: "#f3f6fd",
      display: "flex",
      justifyContent: "space-between",
      flexWrap: "wrap",
    }}
  >
    <RequestCard size="small" number={23} requestType="new" />

    <RequestCard size="small" number={23} requestType="pending" />

    <RequestCard size="small" number={2} requestType="all" />

    <RequestCard size="small" number={2} requestType="clients" />
  </div>
));
