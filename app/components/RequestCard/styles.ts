const styles = (theme: any) => ({
  container: {
    fontFamily: "Poppins",
  },
  large: {
    display: "flex",
    padding: "26px 40px",
    justifyContent: "space-between",
    width: 374,
    backgroundColor: "#FFF",
    "box-shadow": "0 13px 26px 0 rgba(255, 135, 168, 0.1)",
    boxSizing: "border-box",
    borderRadius: 16,
  },
  wrap: {},
  number: { fontWeight: 600, fontSize: 45 },
  type: { fontWeight: 500, fontSize: 18, opacity: 0.5 },
  img: {
    "object-fit": "contain",
    padding: 22,
    backgroundColor: "rgba(233,0,65,0.1)",
    borderRadius: 30,
  },
  small: {
    display: "flex",
    flexFlow: "column",
    padding: 16,
    width: 182,
    backgroundColor: "#FFF",
    "box-shadow": "0 13px 26px 0 rgba(255, 135, 168, 0.1)",
    boxSizing: "border-box",
    borderRadius: 16,
    alignItems: "center",
  },
  wrapSmall: {
    display: "flex",
    flexFlow: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  imgSmall: {
    "object-fit": "contain",
    padding: 12,
    backgroundColor: "rgba(233,0,65,0.1)",
    borderRadius: 16,
    height: 22,
    width: "auto",
  },
  numberSmall: { fontWeight: 600, fontSize: 32 },
  typeSmall: { fontWeight: 500, marginTop: 4, opacity: 0.5 },
});

export default styles;
