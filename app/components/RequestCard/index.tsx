/**
 *
 * RequestCard Component
 *
 */
import React from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";

import classnames from "classnames";
import manyUser from "../../images/manyUser.png";
import newReq from "../../images/newReq.png";
import pendingReq from "../../images/pendingReq.png";
import totalReq from "../../images/totalReq.png";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;

  requestType: string;

  number: number;

  size: string;
};

function RequestCard(props: Props) {
  const {
    classes,
    style,
    className,
    formatMessage,
    size = "large",
    number,
    requestType,
  } = props;

  let img, title;
  if (requestType === "new") {
    img = newReq;
    title = "New Request";
  } else if (requestType === "pending") {
    img = pendingReq;
    title = "Pending Requests";
  } else if (requestType === "all") {
    img = totalReq;
    title = "Total Requests";
  } else if (requestType === "clients") {
    img = manyUser;
    title = "Total Clients";
  }

  return (
    <div className={classnames(classes.container, className)} style={style}>
      {size === "small" ? (
        <div className={classes.small}>
          <div className={classes.wrapSmall}>
            <div className={classes.numberSmall}>
              {number < 10 ? "0" + number : number}
            </div>
            <img src={img} alt={title} className={classes.imgSmall} />
          </div>
          <div className={classes.typeSmall}>{title}</div>
        </div>
      ) : (
        <div className={classes.large}>
          <div className={classes.wrap}>
            <div className={classes.number}>
              {number < 10 ? "0" + number : number}
            </div>
            <div className={classes.type}>{title}</div>
          </div>
          <img src={img} alt={title} className={classes.img} />
        </div>
      )}
    </div>
  );
}

export default withStyles(styles)(withIntl(RequestCard));
