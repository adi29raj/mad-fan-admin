/**
* Chart Messages
*
* This contains all the text for the Chart Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.Chart';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})