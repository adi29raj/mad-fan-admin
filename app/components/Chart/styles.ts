const styles = (theme: any) => ({
  wrapper: {
    width: 374,
    height: 574,
    margin: "0px 26px 0 0px",
    padding: "37px 18.6px 37px 19.3px",
    opacity: 0.77,
    borderRadius: 15,
    boxShadow: "0 13px 26px 0 rgba(255, 135, 168, 0.1)",
    backgroundColor: "#ffffff",
  },
  data: {
    display: "flex",
    flexDirection: "row",
    width: "70%",
    margin: "auto",
  },
  completed: {
    marginRight: 56,
    display: "inline-grid",
  },
  pending: {
    display: "inline-grid",
  },
  name: {
    fontFamily: "Poppins",
    fontSize: 17,
    fontWeight: 500,
    color: "#000000",
    opacity: 0.5

  },
  count: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: 28,
    fontWeight: 600,
    color: " #000000",
  },
  header: {
    fontFamily: "Poppins",
    fontSize: 24,
    fontWeight: 600,
    color: " #000000",
    borderRadius: 4

  },
  green:{
    width: 16,
    height: 16,
    backgroundColor:'#38d271',
    marginLeft:20,
    marginRight:5,
    borderRadius: 4

  },
  yellow:{
    width: 16,
    height: 16,
    backgroundColor:'#fdb428',
    marginLeft:20,
    marginRight:5,
    borderRadius: 4

  },
  red:{
    width: 16,
    height: 16,
    backgroundColor:'#e90041',
    marginLeft:20,
    marginRight:5
  },
  bottom:{
      display:'flex',
      marginLeft:30,
  },
});

export default styles;
