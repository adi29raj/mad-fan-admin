/**
 *
 * Chart Component
 *
 */
import React from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import { PieChart } from "react-minimal-pie-chart";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;
};
const defaultLabelStyle = {
    fontSize: '12px',
  };
  const data=[
    { title: "One", value: 10, color: "#e90041"},
    { title: "Two", value: 15, color: "#38d271" },
    { title: "Three", value: 20, color: "#fdb428" },
  ]
function Chart(props: Props) {
  const { classes, style, className, formatMessage } = props;
  const shiftSize = 7;

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <h3>Order Analytics</h3>
      </div>
      <div className={classes.data}>
        <div className={classes.completed}>
          <span className={classes.count}>05</span>
          <span className={classes.name}>Completed</span>
        </div>
        <div className={classes.pending}>
          <span className={classes.count}>22</span>
          <span className={classes.name}>Pending</span>
        </div>
      </div>
      <div className={classes.chart}>
        <PieChart
          data={data}
          radius={PieChart.defaultProps.radius - shiftSize}
          segmentsShift={(index) => (index === 0 ? shiftSize : 0.5)}
        //   labelStyle={{
        //     ...defaultLabelStyle,
        //   }}
          animate
          labelStyle={{
            fill: '#fff',
            pointerEvents: 'none',
            fontSize:12
        }}
        label={({ dataEntry }) => Math.round(dataEntry.value) + '%'}

            />
      </div>
      <div className={classes.bottom}>
          <div className={classes.green}></div>
          <span>Completed</span>
          <div className={classes.yellow}></div>
          <span>Pending</span>
          <div className={classes.red}></div>
          <span>Rejected</span>

      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(Chart));
