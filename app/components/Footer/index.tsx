/**
 *
 * Footer Component
 *
 */
import React from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";

import classnames from "classnames";

import logo from "../../images/logo2.png";
import google from "../../images/google.png";
import linkedin from "../../images/linkedin.png";
import twitter from "../../images/twitter.png";
import fb from "../../images/fb.png";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;
};

function Footer(props: Props) {
  const { classes, formatMessage, className, style } = props;
  return (
    <div className={classnames(classes.container, className)} style={style}>
      <img className={classes.logo} src={logo} alt="logo" />
      <div className={classes.socials}>
        <div className={classes.social}>
          <img src={google} alt="Google" className={classes.img} />
        </div>
        <div className={classes.social}>
          <img src={fb} alt="Facebook" className={classes.img} />
        </div>
        <div className={classes.social}>
          <img src={linkedin} alt="Linkedin" className={classes.img} />
        </div>
        <div className={classes.social}>
          <img src={twitter} alt="Twitter" className={classes.img} />
        </div>
      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(Footer));
