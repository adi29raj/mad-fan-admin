/**
 *
 * Footer Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Footer from ".";

const stories = storiesOf("Footer", module);

stories.add("default", () => <Footer />);
