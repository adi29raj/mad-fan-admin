/**
* Footer Messages
*
* This contains all the text for the Footer Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.Footer';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})