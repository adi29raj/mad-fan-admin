const styles = {
  container: {
    padding: "10px 40px",
    backgroundColor: "#EA0049",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  logo: {
    height: 45,
    width: "auto",
  },
  socials: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  social: {
    height: 40,
    width: 37,
    "border-radius": "50%",
    // padding:12,
    backgroundColor: "#FFFFFF",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "0px 18px",
  },
  img: {
    height: 18,
    width: 18,
  },

  "@media screen and (max-width: 967px) and (min-width: 0px)": {
    container: {
      flexFlow: "column",
      padding: "30px 60px",
    },
    logo: {
      height: 80,
      width: "auto",
    },
    img: {
      height: 16,
      width: 16,
    },
    social: {
      height: 36,
      width: 36,
      margin: "18px 9px",
    },
  },
};

export default styles;
