/**
* SearchBox Messages
*
* This contains all the text for the SearchBox Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.SearchBox';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})