/**
 *
 * SearchBox Component
 *
 */
import React from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;

  options: any[];

  size: string;

  onChange: Function;
};

const useStyles = makeStyles((theme) => ({
  inputRoot: {
    fontFamily: "Poppins",
    backgroundColor: "#f3f3f3",
    "& .MuiOutlinedInput-notchedOutline": {},
    "&:hover .MuiOutlinedInput-notchedOutline": {},
    "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
      //   border: "1px solid #e90041",
      //   color: "#e90041",
    },
    "&.MuiInputLabel-outlined": {
      fontFamily: "Poppins",
    },
  },
}));

function SearchBox(props: Props) {
  const {
    classes,
    style,
    className,
    formatMessage,
    options,
    size,
    onChange,
  } = props;
  const themeClasses = useStyles();
  return (
    <div className={className} style={style}>
      <Autocomplete
        autoComplete
        size={size}
        id="combo-box-demo"
        classes={themeClasses}
        options={options}
        onChange={onChange}
        disableClearable
        forcePopupIcon={false}
        getOptionLabel={(option) => option.title}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              label="Search Your Celebrity"
              variant="outlined"
              fullWidth
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          );
        }}
      />
    </div>
  );
}

export default withStyles(styles)(withIntl(SearchBox));
