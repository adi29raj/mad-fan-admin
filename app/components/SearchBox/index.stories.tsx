/**
 *
 * SearchBox Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import SearchBox from ".";

const stories = storiesOf("SearchBox", module);

stories.add("default", () => (
  <SearchBox options={["border", "is", "far", "EB"]} />
));
