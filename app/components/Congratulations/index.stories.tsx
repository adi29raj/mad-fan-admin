/**
*
* Congratulations Component
*
*/

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from "@storybook/addon-actions";


import Congratulations from '.';

const stories = storiesOf('Congratulations',module);

stories.add('Challenge',() => <Congratulations type="challenge"  onClose={action("OnClose")}/>);
stories.add('Book Video',() => <Congratulations type="bookvideo"  onClose={action("OnClose")}/>);
stories.add('Coin ',() => <Congratulations type="coin"  onClose={action("OnClose")}/>);
stories.add('Google Contacts',() => <Congratulations type="contact"  onClose={action("OnClose")}/>);