/**
 * 
 * Backdrop Component
 * 
 */
import React from 'react';
import styles from './styles';
import withIntl from '../../utils/withIntl';
import {withStyles} from '@material-ui/core/styles';

type Props = {
    // internal
    classes:any;

    // internal 
    formatMessage:Function;

    // class applied to top level container
    className?:Object;

    // style applied to top level container
    style?: Object;

    // controls backdrop show
    show:boolean;

    // callback when outside backdrop is clicked
    clicked:() => void;
};

function Backdrop(props:Props){
    const {classes,show,clicked} = props;
    return (
        show ? <div className={classes.Backdrop} onClick={clicked}></div> : null
    );
}

export default withStyles(styles)(withIntl(Backdrop));