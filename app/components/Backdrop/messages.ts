/**
 * Backdrop Messages
 * 
 * This contains all the text for the Backdrop Component
 * 
 */

import {defineMessages} from 'react-intl';

export const scope = 'app.component.Backdrop';

export default defineMessages({
    test:{
        id:`${scope}.test`,
        defaultMessage:"Test",
    }
})