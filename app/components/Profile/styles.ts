import bg from "../../images/backgroundprofile.png";

const styles = (theme: any) => ({
  leftwrapper: {
    width: 400,
    height: "auto",
    margin: "32px 33px 0px 60px",
    padding: "46px 73px 0px 42px",
    bordeRadius: 20,
    boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
    backgroundColor: "#ffffff",
    fontFamily: "Poppins",

  },
  rightwrapper: {
    width: 790,
    height: "auto",
    margin: "32px 33px 0 6px",
    padding: "4px 30px 84px 42px",
    bordeRadius: 20,
    boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
    backgroundColor: "#ffffff",
    fontFamily: "Poppins",

  },
  error:{
    color: "#e90041",

  },
  profile: {
    height: 200,
    width: 195,
    backgroundImage: `url(${bg})`,
    "background-repeat": "no-repeat",
    "background-size": "cover",
    "background-position": "center",
    borderRadius: "50%",
    position: "relative",
    objectFit:'cover',
    objectPosition:'top'
  },
  parent: {
    display: "flex",
  },
  innerprofile: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    position: "absolute",
    top: "50%",
    left: "30%",
    fontSize: 58,
    color: "white",
    transform: "translate(50%, -50%)",
  },
  label: {
    margin: "46px 41px 11px 0",
    opacity: "0.5",
    fontSize: 16,
    fontWeight: 600,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.56,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#000000",
  },
  value: {
    marginTop: 0,
    fontSize: 20,
    fontWeight: "bold",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.56,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#000000",
  },
  inputtype: {
    marginTop: 0,
    fontSize: 20,
    fontWeight: "bold",
    fontStretch: "normal",
    border:'none',
    outline:'none',
    fontStyle: "normal",
    lineHeight: 1.56,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#000000",
    marginBottom:30

  },
  data: {
    marginTop: 40,
  },
  button: {
    width: 136,
    height: 42,
    margin: "50px 0px 0 27px",
    padding: "11px 55px 11px 56px",
    borderRadius: 4,
    border: "solid 1px #e90041",
    backgroundColor: "#e90041",
    color: "#ffffff",
    "&:hover": {
      backgroundColor: "#e90041",
    },
  },
  button2: {
    width: 136,
    height: 42,
    margin: "50px 0px 0 27px",
    padding: "11px 55px 11px 56px",
    borderRadius: 4,
    border: "solid 1px #e90041",
    backgroundColor: "#e90041",
    color: "#ffffff",
    "&:hover": {
      backgroundColor: "#e90041",
    },
  },
  button3: {
    width: 136,
    height: 42,
    margin: "50px 0px 0 27px",
    padding: "11px 55px 11px 56px",
    borderRadius: 4,
    border: "solid 1px #e90041",
    backgroundColor: "#e90041",
    color: "#ffffff",
    display: "none",
    "&:hover": {
      backgroundColor: "#e90041",
    },
  },
  buttonwrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    width: 300,
    height: 52,
    border: "none",
    borderRadius: 10,
    backgroundColor: "#f3f3f3",
    outline: "none",
    fontSize: 30,
    marginRight: 12,
  },
  toFrom: { display: "flex", flexFlow: "row" },
  label2: { fontSize: 16, fontWeight: "bold", marginBottom: 8 },
  wrapper: {
    display: "flex",
    flexFlow: "column",
    flex: 1,
    marginRight: 32,
    "&:last-child": { marginRight: 0 },
  },
  inputField: { borderRadius: 10 },
  rightheader: {
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  headertext: {
    fontSize: 30,
    fontWeight: "bold",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.53,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#000000",
  },

  "@media (max-width: 967px)": {
    leftwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
      marginBottom: 30,
    },
    wrapper: {
      marginRight: 0,
    },
    rightwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
    },
    parent: {
      flexDirection: "column",
      width: "100%",
    },
    toFrom: {
      display: "block",
    },
    input: {
      width: "100%",
    },
    headertext: {
      fontSize: 24,
    },
    label: {
      fontSize: 16,
    },
    value: {
      fontSize: 20,
    },
    button2: {
      display: "none",
    },
    button3: {
      display: "block",
      padding: "unset",
      margin: "auto",
      marginTop: 30,
    },
    button: {
      margin: 0,
    },
    icon: {
      position: "absolute",
      top: "50%",
      left: "42%",
      fontSize: 58,
      color: "white",
      transform: "translate(50%, -50%)",
    },
  },
  "@media (max-width: 600px)": {
    leftwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
      marginBottom: 30,
    },
    rightwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
    },
    wrapper: {
      marginRight: 0,
    },
    parent: {
      flexDirection: "column",
      width: "100%",
    },
    toFrom: {
      display: "block",
    },
    input: {
      width: "100%",
    },
    headertext: {
      fontSize: 24,
    },
    label: {
      fontSize: 16,
    },
    value: {
      fontSize: 20,
    },
    button2: {
      display: "none",
    },
    button3: {
      display: "block",
      padding: "unset",
      margin: "auto",
      marginTop: 30,
    },
    button: {
      margin: 0,
    },
    icon: {
      left: "35%",
    },
  },
  "@media (max-width: 400px)": {
    leftwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
      marginBottom: 30,
    },
    rightwrapper: {
      width: "87%",
      height: "auto",
      margin: "auto",
      padding: "15px 20px",
      bordeRadius: 20,
      boxShadow: "0 3px 26px 0 rgba(233, 0, 65, 0.1)",
      backgroundColor: "#ffffff",
    },
    wrapper: {
      marginRight: 0,
    },
    parent: {
      flexDirection: "column",
      width: "100%",
    },
    toFrom: {
      display: "block",
    },
    input: {
      width: "100%",
    },
    headertext: {
      fontSize: 22,
    },
    label: {
      fontSize: 14,
    },
    value: {
      fontSize: 18,
    },
    button2: {
      display: "none",
    },
    button3: {
      display: "block",
      padding: "unset",
      margin: "auto",
      marginTop: 30,
    },
    button: {
      margin: 0,
    },
    icon: {
      left: "30%",
    },
  },
});

export default styles;
