/**
 * Profile Messages
 *
 * This contains all the text for the Profile Component
 *
 */

import { defineMessages } from "react-intl";

export const scope = "app.component.Profile";

export default defineMessages({
  namelabel: {
    id: `${scope}.namelabel`,
    defaultMessage: "Name",
  },
  emaillabel: {
    id: `${scope}.emaillabel`,
    defaultMessage: "Email",
  },
  mobileLabel: {
    id: `${scope}.mobileLabel`,
    defaultMessage: "Mobile",
  },
  profHeader: {
    id: `${scope}.profHeader`,
    defaultMessage: "Professional Details",
  },
  edit: {
    id: `${scope}.edit`,
    defaultMessage: "Edit",
  },

  save: {
    id: `${scope}.save`,
    defaultMessage: "Save",
  },
  videoprice: {
    id: `${scope}.videoprice`,
    defaultMessage: "Video Price",
  },
  autograpgprice: {
    id: `${scope}.autograpgprice`,
    defaultMessage: "Autograph Price",
  },
  error: {
    id: `${scope}.error`,
    defaultMessage: "This is Required",
  },
  selectcategories: {
    id: `${scope}.selectcategories`,
    defaultMessage: "Select Categories",
  },
  selecttag: {
    id: `${scope}.selecttag`,
    defaultMessage: "Select Tags",
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: "Description",
  },
});
