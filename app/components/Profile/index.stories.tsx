/**
 *
 * Profile Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Profile from ".";

const stories = storiesOf("Profile", module);

stories.add("default", () => (
  <Profile
    name="Sharman Joshi"
    email="sharman.joshi@gmail.com"
    mobile="+91 807-5326-697"
    onProfileChange={action("profile change")}
    onEdit={action("edit data")}
    onSave={action("save data")}
  />
));
