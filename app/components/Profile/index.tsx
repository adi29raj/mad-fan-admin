/**
 *
 * Profile Component
 *
 */
import React, { useState } from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import { withStyles } from "@material-ui/core/styles";
import { useForm } from "react-hook-form";
import CameraAltIcon from "@material-ui/icons/CameraAlt";
import { IconButton,} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import Select from "react-select";
import sample from "../../images/salman.png";
import {category} from "./categorydata";
import messages from "./messages";


type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;
  name: string;
  email: string;
  mobile: string;
  onSave: () => void;
  onEdit: () => void;
  onProfileChange: () => void;

};

const colourStyles = {
  control: (styles: any) => ({ ...styles, backgroundColor: "#f2f2f2" }),
  multiValue: (styles: any) => {
    return {
      ...styles,
      backgroundColor: "#ffffff",
      padding: "1px 12px",
      margin: "2px",
    };
  },
  multiValueLabel: (styles: any) => ({
    ...styles,
    fontFamily: "Poppins",
    fontSize: "18px",
    fontWeight: "bold",
    "@media (max-width: 967px)": {
      fontSize: 18,
    },
    "@media (max-width: 600px)": {
      fontSize: 16,
    },
    "@media (max-width: 400px)": {
      fontSize: 14,
    },
  }),
  multiValueRemove: (styles: any) => ({
    ...styles,
    ":hover": {
      color: "black",
    },
  }),
};
function Profile(props: Props) {
  const {
    classes,
    style,
    className,
    formatMessage,
    name,
    email,
    mobile,
    onEdit,
    onProfileChange,
    onSave,
  } = props;
  const {namelabel,emaillabel,mobileLabel,profHeader,description,save,edit,videoprice,autograpgprice,selectcategories,selecttag,error} =messages;
  let [data, setData] = useState({});

  const { register, handleSubmit, watch, errors } = useForm<Inputs>();
  const {
    register: registerForProfessionalForm,
    handleSubmit: handleProfessionalForm,
  } = useForm<Inputs>();

  const onSubmit = (data: any) => {
    onSave(data)
  };

  const handleEdit = (data: any) => {
    onEdit(data)
  };

  const handleSelectForCategory = (newValue: any, actionMeta: any) => {};

  const handleSelectForTags = (newValue: any, actionMeta: any) => {};

  const onImageChange = (event: any) => {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      let file = event.target.files[0];
      onProfileChange(file)

      reader.onloadend = () => {
        setData({
          imagePreview: reader.result,
          file: file,
        });
      }
      reader.readAsDataURL(file);
    }
  };
  return (
    <div className={classes.parent} style={style}>
      <div className={classes.leftwrapper}>
        <div className={classes.innerprofile}>
          <input
            accept="image/*"
            onChange={onImageChange}
            id="contained-button-file"
            name="image"
            type="file"
            hidden
          />
          <label htmlFor="contained-button-file">
            <IconButton component="span">
              <img
                src={data.imagePreview ? data.imagePreview : sample}
                className={classes.profile}
              />
            </IconButton>
            <CameraAltIcon className={classes.icon} />
          </label>
        </div>
        <div className={classes.data}>
          <div>
            <label className={classes.label}>{formatMessage(namelabel)}</label>
            <input
              className={classes.inputtype}
              value={name}
              name="name"
              ref={registerForProfessionalForm}
            />
          </div>
          <div>
            <label className={classes.label}>{formatMessage(emaillabel)}</label>
            <input
              className={classes.inputtype}
              value={email}
              name="email"
              ref={registerForProfessionalForm}
            />
          </div>
          <div>
            <label className={classes.label}>{formatMessage(mobileLabel)}</label>
            <input
              className={classes.inputtype}
              value={mobile}
              name="phone"
              ref={registerForProfessionalForm}
            />
          </div>
        </div>
        <div className={classes.buttonwrapper}>
          <Button
            key={1}
            className={classes.button}
            onClick={handleProfessionalForm(handleEdit)}
          >
            {" "}
            {formatMessage(edit)}
          </Button>
        </div>
      </div>
      <div className={classes.rightwrapper}>
        <div className={classes.rightheader}>
          <div className={classes.headertext}>{formatMessage(profHeader)}</div>
          <div>
            <Button
              className={classes.button2}
              onClick={handleSubmit(onSubmit)}
              key={2}
            >
              {" "}
              {formatMessage(save)}
            </Button>
          </div>
        </div>
        <div>
          <div className={classes.toFrom}>
            <div className={classes.wrapper}>
              <label className={classes.label}>{formatMessage(videoprice)}</label>
              <TextField
                placeholder="Enter Price"
                variant="filled"
                color="secondary"
                InputProps={{
                  classes: { root: classes.inputField },
                }}
                classes={{ root: classes.textField }}
                name="video_price"
                className={classes.input}
                inputRef={register({ required: true })}
              />
              {errors.video_price && (
                <p className={classes.error}> ⚠ {formatMessage(error)}</p>
              )}
            </div>
            <div className={classes.wrapper}>
              <label className={classes.label}>{formatMessage(autograpgprice)}</label>
              <TextField
                placeholder="Enter Price"
                variant="filled"
                color="secondary"
                InputProps={{
                  classes: { root: classes.inputField },
                }}
                classes={{ root: classes.textField }}
                name="auto_price"
                className={classes.input}
                inputRef={register({ required: true })}
              />
              {errors.auto_price && (
                <p className={classes.error}> ⚠ {formatMessage(error)}</p>
              )}
            </div>
          </div>

          <div className={classes.instrn}>
            <div className={classes.wrapper}>
              <label className={classes.label}>{formatMessage(selectcategories)}</label>

              <Select
                defaultValue={[category[2], category[3]]}
                isMulti
                name="category"
                options={category}
                className="basic-multi-select"
                classNamePrefix="select"
                styles={colourStyles}
                inputRef={register({ required: true })}
                onChange={handleSelectForCategory}
              />
              {errors.auto_price && (
                <p className={classes.error}> ⚠ {formatMessage(error)}</p>
              )}
            </div>
          </div>
          <div className={classes.instrn}>
            <div className={classes.wrapper}>
              <label className={classes.label}>{formatMessage(selecttag)}</label>

              <Select
                defaultValue={[category[1]]}
                isMulti
                name="tags"
                options={category}
                className="basic-multi-select"
                classNamePrefix="select"
                styles={colourStyles}
                inputRef={register({ required: true })}
                onChange={handleSelectForTags}
              />
              {/* {errors.auto_price && (
                <p className={classes.error}> ⚠ {formatMessage(error)}</p>
              )} */}
            </div>
          </div>
          <div className={classes.instrn}>
            <div className={classes.wrapper}>
              <label className={classes.label}>{formatMessage(description)}</label>
              <TextField
                multiline
                rows={6}
                placeholder="Type your message here"
                variant="filled"
                color="secondary"
                InputProps={{
                  classes: { root: classes.inputField },
                }}
                classes={{ root: classes.textField }}
                name="message"
                size="small"
                inputRef={register({ required: true })}
              />
              {errors.auto_price && (
                <p className={classes.error}> ⚠ {formatMessage(error)}</p>
              )}
            </div>
          </div>
          <div className={classes.buttonwrapper}>
            <Button
              className={classes.button3}
              onClick={handleSubmit(onSubmit)}
            >
              {" "}
             {formatMessage(save)}
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(Profile));
