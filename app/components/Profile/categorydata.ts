export const category = [
    { value: "actor", label: "Actor" },
    { value: "youtuber", label: "Youtuber" },
    { value: "singer", label: "Singer" },
    { value: "writer", label: "Writer" },
    { value: "localactor", label: "Local Actor" },
  ];