/**
* NotificationPopUp Messages
*
* This contains all the text for the NotificationPopUp Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.NotificationPopUp';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})