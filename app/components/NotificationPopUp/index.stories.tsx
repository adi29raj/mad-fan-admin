/**
 *
 * NotificationPopUp Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import NotificationPopUp from ".";

const stories = storiesOf("NotificationPopUp", module);

const data = [
    {
        time: "2:30 PM",
        title: "booking",
        name: "aditya",
      },
  {
    time: "2:30 PM",
    title: "coin",
  },
  {
    time: "2:30 PM",
    title: "bonus",
  },
  {
    time: "2:30 PM",
    title: "coin",
  },
  {
    time: "2:30 PM",
    title: "coin",
  },
  
  {
    time: "2:30 PM",
    title: "coin",
  },
  {
    time: "2:30 PM",
    title: "coin",
  },
  {
    time: "2:30 PM",
    title: "video",
    name: "aditya",
  },
  {
    time: "2:30 PM",
    title: "booking",
    name: "aditya",
  },
  {
    time: "2:30 PM",
    title: "coin",
  },
];

stories.add("default", () => <NotificationPopUp unread={5} items={data} />);
