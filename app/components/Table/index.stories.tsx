/**
 *
 * Table Component
 *
 */

import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Table from ".";

const stories = storiesOf("Table", module);

stories.add("default", () => (
  <Table
    data={[
      { make: "Toyota", model: "Celica", price: 35000 },
      { make: "Ford", model: "Mondeo", price: 32000 },
      { make: "Porsche", model: "Boxter", price: 72000 },
      { make: "Porsche", model: "Boxter", price: 72000 },
      { make: "Porsche", model: "Boxter", price: 72000 },
    ]}
    headerList={[
      { field: "make", sortable: true, filter: true },
      { field: "model", sortable: true },
      { field: "price", filter: true },
    ]}
  />
));
