const styles = (theme: any) => ({
  container: {
    backgroundColor: "rgba(255,255,255,0.7)",
    "box-shadow": "0 13px 26px 0 rgba(255, 135, 168, 0.1)",
    padding: 36,
    borderRadius: 16,
  },
});

export default styles;
