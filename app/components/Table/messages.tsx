/**
* Table Messages
*
* This contains all the text for the Table Component
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.component.Table';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})