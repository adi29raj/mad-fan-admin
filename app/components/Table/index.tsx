/**
 *
 * Table Component
 *
 */
import React, { useState } from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import classnames from "classnames";

import { AgGridColumn, AgGridReact } from "ag-grid-react";
import "./ag-grid-style.scss";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;

  data: any[];

  headerList: any[];
};

function Table(props: Props) {
  const { classes, style, className, formatMessage, data, headerList } = props;

  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);

  const [rowData, setRowData] = useState(data);

  function onGridReady(params) {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);
  }

  return (
    <div className={classnames(classes.container, className)} style={style}>
      <div className="ag-theme-alpine" style={{ width: "100%", height: 400 }}>
        <AgGridReact
          onGridReady={onGridReady}
          rowData={rowData}
          rowHeight={66}
          // rowSelection="multiple"
        >
          {headerList.map((header) => (
            <AgGridColumn
              field={header.field}
              sortable={header.sortable}
              filter={header.filter}
            ></AgGridColumn>
          ))}
        </AgGridReact>
      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(Table));
