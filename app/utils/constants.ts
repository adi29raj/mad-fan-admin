export const MEDIA_DEVICE_XL = "media-device-xl";
export const MEDIA_DEVICE_DESKTOP = "media-device-desktop";
export const MEDIA_DEVICE_TABLET = "media-device-tablet";
export const MEDIA_DEVICE_MOBILE = "media-device-mobile";

export const BREAKING_POINT = 967;
