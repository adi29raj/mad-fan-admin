import { createStore, applyMiddleware } from "redux";
import { combineReducers, compose } from "redux";
// import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import { fork, all } from "redux-saga/effects";
import "regenerator-runtime/runtime";
import thunk from "redux-thunk";

import searchCelebReducer from "../scenes/SearchCeleb/reducer";
import auth from "../store/reducers/auth";

// to:do combine all saga into
import { searchCelebWatcher } from "../scenes/SearchCeleb/saga";

export type State = any;

const rootReducer = combineReducers({ searchCelebReducer, auth });
const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;

export default createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk, sagaMiddleware))
);

function* rootSaga() {
  yield all([fork(searchCelebWatcher)]);
}

// if (module.hot) {
//   module.hot.accept(() => {});
// }

sagaMiddleware.run(rootSaga);
