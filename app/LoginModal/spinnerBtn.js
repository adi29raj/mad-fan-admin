import React from 'react'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress'
const styles = {
  root: {
    marginLeft: 5
  }
}
const SpinnerAdornment = withStyles(styles)(props => (
  <CircularProgress
    className={props.classes.spinner}
    size={20}
    style={{color:'white'}}
  />
))
const AdornedButton = (props) => {
  const {
    children,
    loading,
    bg,
    ...rest
  } = props
  return (
    <Button type="submit" {...rest} style={{backgroundColor:bg, color:'white'}}>
      {children}
      {loading && <SpinnerAdornment {...rest}  />}
    </Button>
  )
}

export default AdornedButton;