import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
* Direct selector to the celebrity state domain
*/
const selectCelebrityDomain = (state: State) =>
state.Celebrity || initialState;

/**
* Custom selectors to the celebrity state
*/
export const selectCelebrity = createSelector(
selectCelebrityDomain,
(substate) => substate
);