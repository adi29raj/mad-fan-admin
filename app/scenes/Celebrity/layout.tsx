/**
 *
 * Celebrity Component
 *
 */
import React from "react";
import styles from "./styles";
import { useParams } from "react-router-dom";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;
};

function Celebrity(props: Props) {
  const { classes, style, className, formatMessage } = props;
  let { id } = useParams();
  console.log(id);

  return (
    <div className={className} style={style}>
      {formatMessage(messages.header)}
    </div>
  );
}

export default withStyles(styles)(withIntl(Celebrity));
