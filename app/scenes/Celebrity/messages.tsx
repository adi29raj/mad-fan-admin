/**
* Celebrity Messages
*
* This contains all the text for the Celebrity Scene
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.scene.Celebrity';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})