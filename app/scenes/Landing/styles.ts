const styles = {
  container: {
    //  fontFamily:'Raleway',
    fontFamily: "Raleway",
    fontSize: "50%",
  },
  chance: {},
  smallChance: {},
  social: {
    marginTop: 80,
  },
  smallSocial: {
    marginTop: 60,
  },
  works: {
    marginTop: 90,
  },
  faq: {
    marginTop: 110,
  },
  slider: {
    marginTop: 102,
  },
  footer: {
    marginTop: 144,
  },

  "@media screen and (max-width: 600px) and (min-width: 0px)": {
    container: {
      "font-size": "40%",
    },
    slider: {
      marginTop: 51,
    },
    footer: {
      marginTop: 65,
    },
  },

  "@media screen and (max-width: 967px) and (min-width: 601px)": {
    container: {
      "font-size": "50%",
    },
    slider: {
      marginTop: 102,
    },
    footer: {
      marginTop: 144,
    },
  },

  "@media screen and (max-width: 1600px) and (min-width: 968px)": {
    container: {
      "font-size": "50%",
    },
    slider: {
      marginTop: 102,
    },
    footer: {
      marginTop: 144,
    },
  },
};
export default styles;
