import React, { useState, useEffect } from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import withIntl from "../../utils/withIntl";
import { SizeMe } from "react-sizeme";

import axios from "../../shared/axios-details";

import Chance from "../../Landing/Chance";
import SmallChance from "../../Landing/SmallChance";
import Social from "../../Landing/Social";
import SmallSocial from "../../Landing/SmallSocial";
import Works from "../../Landing/Works";
import Faq from "../../Landing/FAQ";
import Slider from "../../Landing/Slider";
import Footer from "../../components/Footer";

const BREAKING_POINT = process.env.REACT_APP_SIZE_ME
  ? process.env.REACT_APP_SIZE_ME
  : 967;

type Props = {
  classes: any;
};

function Landing(props: Props) {
  const { classes } = props;
  const [influencer, setInfluencer] = useState<any[]>([]);
  //   useEffect(() => {
  //     const apiData = {
  //       info_type: 1,
  //     };
  //     axios
  //       .post("/influencer.php", apiData)
  //       .then((res) => {
  //         if (res.data.status) {
  //           console.log(res.data);
  //           setInfluencer(res.data.data);
  //         } else {
  //           console.log(res.data);
  //         }
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //       });
  //   }, []);
  return (
    <SizeMe>
      {({ size }: { size: any }) => (
        <div className={classes.container}>
          {size?.width > BREAKING_POINT ? (
            <Chance className={classes.chance} />
          ) : (
            <SmallChance className={classes.smallChance} />
          )}
          {/* {size?.width > BREAKING_POINT ? (
            <Social influencer={influencer} className={classes.social} />
          ) : (
            <SmallSocial
              influencer={influencer}
              className={classes.smallSocial}
            />
          )} */}
          <Works className={classes.works} />
          <Faq className={classes.faq} />
          <Slider className={classes.slider} />
          <Footer className={classes.footer} />
        </div>
      )}
    </SizeMe>
  );
}

export default withStyles(styles)(withIntl(Landing));
