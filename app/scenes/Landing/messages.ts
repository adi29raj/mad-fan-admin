/**
 * Landing Messages
 *
 * This contains all the text for the Logo Component
 *
 */

import { defineMessages } from "react-intl";

export const scope = "app.component.Landing";

export default defineMessages({
  test: {
    id: `${scope}.test`,
    defaultMessage: "Test",
  },
});
