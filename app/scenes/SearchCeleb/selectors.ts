import { createSelector } from "reselect";
import { initialState } from "./reducer";
import { State } from "../../utils/configureStore";

/**
 * Direct selector to the searchCeleb state domain
 */
const selectSearchCelebDomain = (state: State) =>
  state.searchCelebReducer || initialState;

/**
 * Custom selectors to the searchCeleb state
 */
export const selectSearchCeleb = createSelector(
  selectSearchCelebDomain,
  (substate) => substate
);

export const selectLoading: (arg0: State) => boolean = createSelector(
  selectSearchCeleb,
  (sc) => sc.loading
);

export const selectCategories: (arg0: State) => boolean = createSelector(
  selectSearchCeleb,
  (sc) => sc.categories
);

export const selectLanguages: (arg0: State) => boolean = createSelector(
  selectSearchCeleb,
  (sc) => sc.languages
);

export const selectInfluencers: (arg0: State) => boolean = createSelector(
  selectSearchCeleb,
  (sc) => sc.influencers
);
