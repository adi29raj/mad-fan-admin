/**
 * SearchCeleb Messages
 *
 * This contains all the text for the SearchCeleb Component
 *
 */

import { defineMessages } from "react-intl";

export const scope = "app.component.SearchCeleb";

export default defineMessages({
  results: {
    id: `${scope}.results`,
    defaultMessage: "Results",
  },
});
