/**
 *
 * SearchCeleb Component
 *
 */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import Button from "../../components/Button";
import MuiButton from "@material-ui/core/Button";
import ImageCard from "../../components/ImageCard";
import Filter, { FilterProp } from "../../components/Filter";
import SearchBox from "../../components/SearchBox";
import { faAngleDown, faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Popover from "@material-ui/core/Popover";
import { useWindowWidth } from "@react-hook/window-size";
import { BREAKING_POINT, LIMIT_LARGE_CATEGORIES } from "./constants";
import { Influencer } from "./reducer";
import {
  MEDIA_DEVICE_DESKTOP,
  MEDIA_DEVICE_MOBILE,
} from "../../utils/constants";
import { cloneDeep } from "lodash";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;

  loading: boolean;

  categories: any;

  languages: { id: number; language: string }[];

  influencers: Influencer[];

  load: (value: boolean) => void;
};

function SearchCeleb(props: Props) {
  const {
    classes,
    style,
    className,
    formatMessage,
    loading,
    categories,
    languages,
    influencers,
    load,
  } = props;

  useEffect(() => {
    load(true);
  }, []);

  const history = useHistory();

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1400 },
      items: 6,
      slidesToSlide: 3, // optional, default to 1.
    },
    tablet: {
      breakpoint: { max: 1400, min: 967 },
      items: 5,
      slidesToSlide: 5, // optional, default to 1.
    },
    mobile: {
      breakpoint: { max: 967, min: 0 },
      items: 3,
      slidesToSlide: 1, // optional, default to 1.
    },
  };

  // Popover handlers;
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handlePopover = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = !!anchorEl;
  const id = open ? "simple-popover" : undefined;

  const mediaDevice =
    useWindowWidth() > BREAKING_POINT
      ? MEDIA_DEVICE_DESKTOP
      : MEDIA_DEVICE_MOBILE;

  const btnSize =
    mediaDevice === MEDIA_DEVICE_DESKTOP &&
    categories?.length <= LIMIT_LARGE_CATEGORIES
      ? "regular"
      : "small";

  const [catgs, setCatgs] = useState(categories);
  useEffect(() => {
    setCatgs(categories);
  }, [categories]);

  const applyFilter = (target: any[], filters: any[], basis: any) => {
    const updatedTarget: any[] = [];
    target.forEach((tar) => {
      if (filters.every((val) => tar[basis]?.includes(val))) {
        updatedTarget.push(cloneDeep(tar));
      }
    });
    return updatedTarget;
  };

  const filterByCategory = (catgList: any[]) => {
    const selectedCatgs = catgList
      .filter((catg) => {
        if (!!catg?.selected) return catg.name;
      })
      .map((el) => el.name);

    if (selectedCatgs?.length) {
      setInfluencerList(applyFilter(influencers, selectedCatgs, "category"));
    } else {
      setInfluencerList(influencers);
    }
  };

  const onCategoryClick = (id: string | number | null | undefined) => {
    const updatedCatgs = catgs.map((cg: { id: any; selected: any }) => {
      if (cg.id === id) {
        return { ...cg, selected: !cg.selected };
      }
      return cg;
    });
    filterByCategory(updatedCatgs);
    setFilters(clearFilters());
    setCatgs(updatedCatgs);
  };

  const clearFilters = () => {
    const filterList = [];
    // Adding Gender filters
    filterList.push({
      filterSection: "Gender",
      filterType: "checkbox",
      filters: [
        { id: 0, name: "Male", checked: false, disabled: false },
        { id: 1, name: "Female", checked: false, disabled: false },
      ],
    });

    // Adding language filters
    if (languages?.length) {
      const filter = {
        filterSection: "Languages",
        filterType: "checkbox",
        filters: languages.map((el) => ({
          id: el.id,
          name: el.language,
          checked: false,
          disabled: false,
        })),
      };
      filterList.push(filter);
    }
    return filterList;
  };

  const [filters, setFilters] = useState<FilterProp[]>([]);
  useEffect(() => {
    setFilters(clearFilters());
  }, [languages]);

  const onFiltersChange = (filterList: { filters: any[] }[]) => {
    setFilters(filterList);
    let updatedList = influencers;
    // [0] - Gender
    const selectedGenders = filterList[0].filters
      .filter((el: { checked: any }) => el.checked)
      .map((el: { name: any }) => el.name);

    if (selectedGenders?.length) {
      const list: Influencer[] = [];
      updatedList.forEach((inf) => {
        if (selectedGenders.includes(inf?.gender)) {
          list.push(cloneDeep(inf));
        }
      });
      updatedList = list;
    }

    // [1] - Language
    const selectedLanguage = filterList[1].filters
      .filter((el: { checked: any }) => el.checked)
      .map((el: { name: any }) => el.name);

    if (selectedLanguage?.length) {
      const list = [];
      updatedList.forEach((inf) => {
        if (selectedLanguage.some((el) => inf.language.includes(el))) {
          list.push(cloneDeep(inf));
        }
      });
      updatedList = list;
    }
    if (updatedList?.length) {
      setInfluencerList(updatedList);
      setCatgs(categories);
    }
  };

  const [influencerList, setInfluencerList] = useState(influencers);
  useEffect(() => {
    setInfluencerList(influencers);
  }, [influencers]);

  return loading ? (
    <div>Loading</div>
  ) : (
    <div className={classnames(className, classes.container)} style={style}>
      <div className={classes.categories}>
        <Carousel
          swipeable={true}
          draggable={true}
          responsive={responsive}
          // ssr={true} // means to render carousel on server-side.
          infinite={true}
          // autoPlay={this.props.deviceType !== "mobile" ? true : false}
          // autoPlaySpeed={1000}
          keyBoardControl={true}
          customTransition="all .5"
          transitionDuration={500}
          containerClass="carousel-container"
          removeArrowOnDeviceType={["tablet", "mobile", "desktop"]}
          // deviceType={this.props.deviceType}
          dotListClass="custom-dot-list-style"
          itemClass="carousel-item-padding-40-px"
        >
          <Button
            name="Home"
            key={0}
            buttonType="primary"
            selected
            buttonSize={btnSize}
            onClick={() => {}}
            className={classes.catBtn}
            leftIcon={faAngleLeft}
          />
          {/* <span
              className={classes.verticalLine}
              style={{ height: btnSize === "small" ? 30 : 60 }}
            ></span> */}
          {catgs.map(
            (cg: {
              name: string;
              id: string | number | null | undefined;
              selected: boolean | undefined;
            }) => (
              <Button
                name={cg.name}
                key={cg.id}
                buttonType="primary"
                buttonSize={btnSize}
                onClick={() => onCategoryClick(cg.id)}
                className={classes.catBtn}
                selected={cg?.selected}
              />
            )
          )}
        </Carousel>
      </div>

      <div className={classes.content}>
        {mediaDevice === MEDIA_DEVICE_DESKTOP ? (
          <div className={classes.wrapper}>
            <Filter
              className={classes.filters}
              filterList={filters}
              onChange={onFiltersChange}
            />
            <section className={classes.celebContainer}>
              <div className={classes.wrap}>
                <div className={classes.results}>
                  {formatMessage(messages.results)}
                  <span className={classes.noOfResult}>
                    ({influencerList.length})
                  </span>
                </div>
                <SearchBox
                  className={classes.serachBox}
                  size="small"
                  options={influencers.map((inf) => ({
                    id: inf.id,
                    title: inf.name,
                  }))}
                  onChange={(event, value, reason) => {
                    if (reason === "select-option") {
                      history.push(`/celebrity/${value.id}`);
                    }
                  }}
                />
              </div>
              <div className={classes.celebs}>
                {influencerList?.map(
                  (inf) =>
                    inf && (
                      <div className={classes.card}>
                        <ImageCard
                          img={inf?.profile_photo}
                          alt={inf?.id}
                          price={parseInt(inf?.rate) || 0} // to:do remove 0
                          celebName={inf?.name}
                          famousFor={inf?.familiar_for}
                          isHoverable={mediaDevice === MEDIA_DEVICE_DESKTOP}
                          onClick={() => history.push(`/celebrity/${inf.id}`)}
                          onBook={() => history.push(`/celebrity/${inf.id}`)}
                          onViewProfile={() =>
                            history.push(`/celebrity/${inf.id}`)
                          }
                        />
                      </div>
                    )
                )}
              </div>
            </section>
          </div>
        ) : (
          <div className={classes.celebContainerSmall}>
            <div className={classes.wrapSmall}>
              <div className={classes.resultsSmall}>
                {formatMessage(messages.results)}
                <span className={classes.noOfResultSmall}>
                  ({influencerList.length})
                </span>
              </div>
              <MuiButton
                className={classes.filterButton}
                size="small"
                endIcon={<FontAwesomeIcon icon={faAngleDown} />}
                onClick={handlePopover}
              >
                Filter
              </MuiButton>
            </div>
            <SearchBox
              size="small"
              options={influencers.map((inf) => ({
                id: inf.id,
                title: inf.name,
              }))}
              onChange={(event, value, reason) => {
                if (reason === "select-option") {
                  history.push(`/celebrity/${value.id}`);
                }
              }}
              className={classes.smallSearchBox}
            />
            <div className={classes.celebsSmall}>
              {influencerList?.map(
                (inf) =>
                  inf && (
                    <div className={classes.cardSmall}>
                      <ImageCard
                        img={inf?.profile_photo}
                        alt={inf?.id}
                        size="small"
                        price={parseInt(inf?.rate) || 0} // to:do remove 0
                        celebName={inf?.name}
                        famousFor={inf?.familiar_for}
                        onClick={() => history.push(`/celebrity/${inf.id}`)}
                        onBook={() => history.push(`/celebrity/${inf.id}`)}
                        onViewProfile={() =>
                          history.push(`/celebrity/${inf.id}`)
                        }
                      />
                    </div>
                  )
              )}
            </div>
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center",
              }}
            >
              <Filter
                className={classes.filters}
                style={{
                  maxWidth: 200,
                  marginRight: 0,
                }}
                filterList={filters}
                onChange={onFiltersChange}
              />
            </Popover>
          </div>
        )}
      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(SearchCeleb));
