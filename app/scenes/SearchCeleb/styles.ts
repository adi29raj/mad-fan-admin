const styles = (theme: any) => ({
  container: {
    width: "90%",
    margin: "auto",
    fontFamily: "Poppins",
    paddingBottom: 36,
  },
  categories: {
    marginTop: 28,
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    // flexWrap: "wrap",
  },
  verticalLine: {
    borderLeft: "2px solid rgba(0,0,0,0.14)",
    height: "60px",
    margin: "0px 8px",
  },
  catBtn: { margin: 8 },
  content: {
    marginTop: 56,
  },
  wrapper: { display: "flex" },
  filters: {
    width: 300,
    marginRight: 36,
    boxSizing: "content-box",
    height: "100%",
  },
  celebContainer: {
    width: "100%",
  },
  wrap: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  results: {
    fontFamily: 24,
    fontWeight: 600,
    marginLeft: 8,
  },
  noOfResult: { fontWeight: "normal", opacity: 0.5 },
  serachBox: { marginRight: 40, width: "100%", maxWidth: 342 },
  celebs: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: 16,
    justifyContent: "flex-start",
  },
  card: { marginTop: 12, margin: 8 },
  "@media (max-width: 967px)": {
    container: { width: "100%" },
    categories: {
      // flexWrap: "nowrap",
      // overflow: "hidden",
    },
    content: {
      marginTop: 32,
    },
    celebContainerSmall: {
      width: "100%",
    },
    smallSearchBox: {
      width: "90%",
      margin: "auto",
      marginTop: 16,
      marginBottom: 16,
    },
    wrapSmall: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      width: "90%",
      margin: "auto",
    },
    resultsSmall: {
      fontFamily: 20,
      fontWeight: 600,
      // marginLeft: 8,
    },
    noOfResult: { fontWeight: "normal", opacity: 0.5 },
    filterButton: {
      fontSize: 14,
      fontWeight: "500",
      fontFamily: "Poppins",
      textTransform: "none",
    },
    celebsSmall: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
    },
    cardSmall: { marginTop: 12, margin: 8 },
  },
});

export default styles;
