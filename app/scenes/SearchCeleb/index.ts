/**
 *
 * SearchCeleb
 *
 */
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose, bindActionCreators } from "redux";

import {
  selectLoading,
  selectCategories,
  selectLanguages,
  selectInfluencers,
} from "./selectors";

import Layout from "./layout";
import reducer from "./reducer";

import { load, setCategories } from "./actions";

const mapStateToProps = createStructuredSelector({
  loading: selectLoading,
  categories: selectCategories,
  languages: selectLanguages,
  influencers: selectInfluencers,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      load,
      setCategories,
    },
    dispatch
  );

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(Layout);
