import React, { Suspense } from "react";
import { Helmet } from "react-helmet";
import { Switch, Route } from "react-router-dom";
import Main from "../../scenes/Main/index";
// import Landing from "../Landing";
const Landing = React.lazy(() => {
  return import("../Landing");
});

export default function App() {
  return (
    <>
      <Helmet defaultTitle="MadFan">
        <meta name="description" content="Mad fan by Codebucket" />
      </Helmet>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route
            path="/"
            exact
            render={(props: any) => <Landing {...props} />}
          />
          <Route path="*" render={(props: any) => <Main {...props} />} />
        </Switch>
      </Suspense>
    </>
  );
}
