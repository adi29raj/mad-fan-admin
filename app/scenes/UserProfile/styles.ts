const styles = (theme: any) => ({
  container: {
    width: "95%",
    margin: "auto",
    display: "flex",
    marginTop: 26,
  },
  userProfileCard: { minWidth: 300, marginRight: 36, boxSizing: "content-box" },
  wrapper: { display: "flex", flexFlow: "column" },
  referAndEarn: {},
  content: { marginTop: 48 },
});

export default styles;
