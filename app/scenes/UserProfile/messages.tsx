/**
* UserProfile Messages
*
* This contains all the text for the UserProfile Scene
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.scene.UserProfile';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})