/**
 *
 * UserProfile constants
 *
 */

const scene = "app/UserProfile";
export const DEFAULT_ACTION = `${scene}/DEFAULT_ACTION`;

export const BREAKING_POINT = 967;
