/**
 *
 * UserProfile Component
 *
 */
import React, { lazy, Suspense } from "react";
import styles from "./styles";
import withIntl from "../../utils/withIntl";
import messages from "./messages";
import { withStyles } from "@material-ui/core/styles";
import { Switch, Route, useHistory } from "react-router-dom";
import classnames from "classnames";
import UserProfileCard from "../../components/UserProfile";
import ReferAndEarn from "../../components/ReferAndEarn";
import { useWindowWidth } from "@react-hook/window-size";
import { BREAKING_POINT } from "./constants";
import {
  MEDIA_DEVICE_DESKTOP,
  MEDIA_DEVICE_MOBILE,
} from "../../utils/constants";

import sample from "../../images/sample.png";

type Props = {
  /** internal */
  classes: any;

  /** internal */
  formatMessage: Function;

  /** class applied to top level container */
  className?: any;

  /** style applied to top level container */
  style?: Object;
};

function UserProfile(props: Props) {
  const { classes, style, className, formatMessage } = props;

  const history = useHistory();

  const mediaDevice =
    useWindowWidth() > BREAKING_POINT
      ? MEDIA_DEVICE_DESKTOP
      : MEDIA_DEVICE_MOBILE;

  return (
    <div className={classnames(className, classes.container)} style={style}>
      {mediaDevice === MEDIA_DEVICE_DESKTOP && (
        <UserProfileCard
          usedAsDrawer={false}
          className={classes.userProfileCard}
          dp={sample}
          userName="Rahul Saxena"
          level={1}
          onEditProfile={() => {}}
          onChangeRoute={(route) => history.push(`/profile/${route}`)}
        />
      )}
      <div className={classes.wrapper}>
        <div className={classes.referAndEarn}>
          <ReferAndEarn link="http://www.madfan.com/098weiaQ/03" />
        </div>
        <div className={classes.content}>
          <Switch>
            <Suspense fallback={() => {}}>
              <Route
                path="/profile/bookings"
                render={(componentProps) => <div>My Bookings</div>}
              />
              <Route
                path="/profile/win-coins"
                render={(componentProps) => <div> Win Coins</div>}
              />
            </Suspense>
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default withStyles(styles)(withIntl(UserProfile));
