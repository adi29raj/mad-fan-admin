import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
* Direct selector to the userProfile state domain
*/
const selectUserProfileDomain = (state: State) =>
state.UserProfile || initialState;

/**
* Custom selectors to the userProfile state
*/
export const selectUserProfile = createSelector(
selectUserProfileDomain,
(substate) => substate
);