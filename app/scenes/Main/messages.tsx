/**
* Main Messages
*
* This contains all the text for the Main Scene
*
*/

import {defineMessages} from 'react-intl';

export const scope = 'app.scene.Main';

export default defineMessages({
header:{
id:`${scope}.header`,
defaultMessage:"Test",
}
})