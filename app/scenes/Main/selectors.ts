import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the main state domain
 */
const selectMainDomain = (state: State) => state.Main || initialState;

/**
 * Custom selectors to the main state
 */
export const selectMain = createSelector(
  selectMainDomain,
  (substate) => substate
);

export const selectToken = (state) => state.auth.token;
