import React, { useState, useEffect, lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import AppBar from "../../components/AppBar";
import UserProfile from "../UserProfile";
import LoginModal from "../../LoginModal";
import {
  MEDIA_DEVICE_DESKTOP,
  MEDIA_DEVICE_MOBILE,
  BREAKING_POINT,
} from "../../utils/constants";
import { useWindowWidth } from "@react-hook/window-size";

const SearchCeleb = lazy(() => import("../SearchCeleb"));
const Celebrity = lazy(() => import("../Celebrity"));

function Main(props: any) {
  //   const { load } = props;

  //   useEffect(() => {
  //     // load(true);
  //   }, []);

  const mediaDevice =
    useWindowWidth() > BREAKING_POINT
      ? MEDIA_DEVICE_DESKTOP
      : MEDIA_DEVICE_MOBILE;

  const [loginModal, setLoginModal] = useState(false);

  return (
    <>
      <div id="root">
        <AppBar
          device={mediaDevice === MEDIA_DEVICE_DESKTOP ? "large" : "small"}
          onLogin={() => setLoginModal(true)}
          isLoggedIn={!!props.token}
        />
        <LoginModal
          handleClose={() => setLoginModal(false)}
          open={loginModal}
        />
        <Switch>
          <Suspense fallback={() => {}}>
            <Route
              path="/search"
              render={(componentProps) => <SearchCeleb {...componentProps} />}
            />
            <Route
              path="/celebrity/:id"
              render={(componentProps) => <Celebrity {...componentProps} />}
            />
            <Route
              path="/profile"
              render={(componentProps) => <UserProfile {...componentProps} />}
            />
          </Suspense>
        </Switch>
      </div>
    </>
  );
}

export default Main;
